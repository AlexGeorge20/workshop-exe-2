module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
   // textColor: theme => theme('colors'),
    textColor: {
        
      headline1:'#ffffff',
      headline2:'#d9dae4',
      footercolor:'#ff6e43',
      textwhite:'#ffffff',
      formgrey:'#8c9dc5',
      section3subheading:'#111111',
      section3sub:'#343434',
      section2black:'#242635',
      try:'#0b0f15',
      lastseccolor:'#323232',
    },
    colors:{
      backgroundorange:'#ff6e43',
      bordergrey:'#e0dfdf',
      footergrey:'#5f5f5f',
      buttonbg:'#ffffff',
      formborder:'#2178ff',
      
    },
    
    }, 
  },
  
  plugins: [],
}
